apt-get update

#Insall some shit
apt-get install build-essential zlib1g zlib1g-dev libreadline6 libreadline6-dev libssl-dev g++ libyaml-dev libzlcore-dev curl git imagemagick sendmail-bin vim -y
#Setting up colored vim
sed -i 's/^"syntax .*/syntax on/' /etc/vim/vimrc
echo "Installing Ruby"

#Donloading and installing ruby with RVM
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
curl -sSL https://get.rvm.io | bash -s stable
source /etc/profile.d/rvm.sh
rvm install 2.1
gem i bundler

echo "Installing Passenger and nginx"

gem i passenger
apt-get install libcurl4-openssl-dev -y
passenger-install-nginx-module

## Make nginx a startable demon

#create the startup file
vim /etc/init.d/nginx
#and ipunt following: 

#! /bin/sh

### BEGIN INIT INFO
# Provides:          nginx
# Required-Start:    $all
# Required-Stop:     $all
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: starts the nginx web server
# Description:       starts nginx using start-stop-daemon
### END INIT INFO

PATH=/opt/nginx/sbin:/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/opt/nginx/sbin/nginx
NAME=nginx
DESC=nginx

test -x $DAEMON || exit 0

# Include nginx defaults if available
if [ -f /etc/default/nginx ] ; then
        . /etc/default/nginx
fi

set -e

case "$1" in
  start)
        echo -n "Starting $DESC: "
        start-stop-daemon --start --quiet --pidfile /opt/nginx/logs/$NAME.pid \
                --exec $DAEMON -- $DAEMON_OPTS
        echo "$NAME."
        ;;
  stop)
        echo -n "Stopping $DESC: "
        start-stop-daemon --stop --quiet --pidfile /opt/nginx/logs/$NAME.pid \
                --exec $DAEMON
        echo "$NAME."
        ;;
  restart|force-reload)
        echo -n "Restarting $DESC: "
        start-stop-daemon --stop --quiet --pidfile \
                /opt/nginx/logs/$NAME.pid --exec $DAEMON
        sleep 1
        start-stop-daemon --start --quiet --pidfile \
                /opt/nginx/logs/$NAME.pid --exec $DAEMON -- $DAEMON_OPTS
        echo "$NAME."
        ;;
  reload)
          echo -n "Reloading $DESC configuration: "
          start-stop-daemon --stop --signal HUP --quiet --pidfile     /opt/nginx/logs/$NAME.pid \
              --exec $DAEMON
          echo "$NAME."
          ;;
      *)
            N=/etc/init.d/$NAME
            echo "Usage: $N {start|stop|restart|reload|force-reload}" >&2
         exit 1
            ;;
    esac

    exit 0
# END
chown root:root /etc/init.d/nginx
chmod +x /etc/init.d/nginx
/usr/sbin/update-rc.d -f nginx defaults

/etc/init.d/nginx start

#Installing more stuff
apt-get install mysql-server libmysqlclient-dev  -y
apt-get install proftpd -y
#to add FTP USERS
#mkdir editor_uploads
#chmod -R 777 editor_uploads/
#useradd selin -s /sbin/nologin -m -d /home/sites/selin/public/uploads/editor_uploads
#echo -e "7ecaza7e\n7ecaza7e\n" | passwd selin


#Now clone your git repo, think a deploy system, remember to give all permissions and use this example to setup /opt/nginx/conf/nginx.conf
# server {
#           server_name followme.virtuoid.net;
#           listen 80;
#           root /home/webapps/followme/public;
#           passenger_enabled on;
#         }


#Install java
apt-get install -y openjdk-7-jdk

#Install elastic search
wget https://download.elastic.co/elasticsearch/elasticsearch/elasticsearch-1.7.1.zip
mkdir /home/elasticsearch
unzip elasticsearch-1.7.1.zip
mv elasticsearch-1.7.1/* /home/elasticsearch
sed -i 's/^#cluster.name: .*/cluster.name: elasticsearch_frankswing/' /home/elasticsearch/config/elasticsearch.yml
sed -i 's/^#node.name: .*/node.name: "frank_swing"/' /home/elasticsearch/config/elasticsearch.yml
sed -i 's/^#network.host: .*/network.host: 127.0.0.1/' /home/elasticsearch/config/elasticsearch.yml