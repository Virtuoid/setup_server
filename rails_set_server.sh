adduser virtuoid

# Shitty starter
sudo apt-get update
sudo apt-get -y install screen
screen

sudo apt-get install -y curl gnupg build-essential vim git mysql-server libmysqlclient-dev 

#make vim nicer
sudo sed -i 's/^"syntax .*/syntax on/' /etc/vim/vimrc

# GET and confire ruby with RVM
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
\curl -sSL https://get.rvm.io | bash -s stable --ruby
sudo usermod -a -G rvm `whoami`

if sudo grep -q secure_path /etc/sudoers; then sudo sh -c "echo export rvmsudo_secure_path=1 >> /etc/profile.d/rvm_secure_path.sh" && echo Environment variable installed; fi

rvm install ruby-2.2.1
rvm --default use ruby-2.2.1

#It's time for bundler
gem install bundler --no-rdoc --no-ri


#For rails need node.js
sudo apt-get update &&
sudo apt-get install -y apt-transport-https ca-certificates &&
curl --fail -ssL -o setup-nodejs https://deb.nodesource.com/setup_0.12 &&
sudo bash setup-nodejs &&
sudo apt-get install -y nodejs

# Install our PGP key and add HTTPS support for APT
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 561F9B9CAC40B2F7
sudo apt-get install -y apt-transport-https ca-certificates

# Add our APT repository
sudo sh -c 'echo deb https://oss-binaries.phusionpassenger.com/apt/passenger wheezy main > /etc/apt/sources.list.d/passenger.list'
sudo apt-get update

# Install Passenger + Nginx
sudo apt-get install -y nginx-extras passenger

# EDIT /etc/nginx/nginx.conf and uncomment
passenger_root /some-filename/locations.ini;
passenger_ruby /usr/bin/passenger_free_ruby;


sudo service nginx restart


# DEPLOY

#create a folder and git clone there the application
#Edit config/database.yml with datas form mysql
#Now run:
bundle exec rake secret
#copy the output and paste in confid/secrets.yml in the Production area
#Now compile Assets, migrate db and set Rails to production
bundle exec rake assets:precompile db:migrate RAILS_ENV=production

#Configure NGIX:
passenger-config about ruby-command
#Copy the 'Command' row

sudo vim /etc/nginx/sites-enabled/myapp.conf
server {
    listen 80;
    server_name yourserver.com;

    # Tell Nginx and Passenger where your app's 'public' directory is
    root /var/www/myapp/code/public;

    # Turn on Passenger
    passenger_enabled on;
    #The value of Command
    passenger_ruby /path-to-ruby;
}


sudo service nginx restart

#Transfer latest code
https://www.phusionpassenger.com/library/walkthroughs/deploy/ruby/ownserver/nginx/oss/deploy_updates.html

#If you like remember to edit the hostname

sudo echo 'baal.virtuoid.net' > /etc/hostname
sudo hostname 'baal.virtuoid.net'


sudo apt-get -qy install proftpd <<**
standalone

**

sysv-rc-conf proftpd --level 2 on
/etc/init.d/proftpd start

useradd virtuoid_ftp -s /sbin/nologin -m -d /home/sites
echo -e "Antanipuppolini1\nAntanipuppolini1\n" | passwd virtuoid_ftp

sed -i '/^#\sDefaultRoot/s/^#//' /etc/proftpd/proftpd.conf
sed -i '/^#\sRequireValidShell/s/^#//' /etc/proftpd/proftpd.conf
service proftpd restart